<?php

namespace Jsrx\JdVopSdk\Service;

use Psr\SimpleCache\InvalidArgumentException;
use Throwable;

/**
 * VOP-商品API
 * @class Goods
 * @package Jsrx\JdVopSdk\Service
 */
class Goods extends BasicService
{
    /**
     * 查询商品池编号
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function getSkuPoolInfo()
    {
        $method = 'jingdong.vop.goods.getSkuPoolInfo';
        return $this->sendRequest($method);
    }

    /**
     * 查询池内商品编号
     * @param $bizPoolId
     * @param $pageSize
     * @param int $offset
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function querySkuByPage($bizPoolId, $pageSize, $offset = 0)
    {
        $method = 'jingdong.vop.goods.querySkuByPage';
        return $this->sendRequest($method, compact('bizPoolId', 'offset', 'pageSize'));
    }

    /**
     * 商品可采校验接口
     * @param $skuNumInfoList
     * @param $areaInfo
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function getSkusAllSaleState($skuNumInfoList, $areaInfo)
    {
        $method = 'jingdong.vop.goods.getSkusAllSaleState';
        return $this->sendRequest($method, ["getStockByIdGoodsReq" => compact('skuNumInfoList', 'areaInfo')]);
    }

    /**
     * 获取商品详情
     * @param $skuId
     * @param $queryExtSet
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function getSkuDetailInfo($skuId, $queryExtSet)
    {
        $method = 'jingdong.vop.goods.getSkuDetailInfo';
        return $this->sendRequest($method, compact('skuId', 'queryExtSet'));
    }

    /**
     * 搜索商品
     * @param $searchParams
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function searchSku($searchParams)
    {
        $method = 'jingdong.vop.goods.searchSku';
        return $this->sendRequest($method, $searchParams);
    }

    /**
     * 批量验证货到付款
     * @param $skuIdList
     * @param $areaInfo
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function getCanCodList($skuIdList, $areaInfo)
    {
        $method = 'jingdong.vop.goods.getCanCodList';
        return $this->sendRequest($method, compact('skuIdList', 'areaInfo'));
    }

    /**
     * 查询商品区域购买限制
     * @param $params
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function checkAreaLimitList($params)
    {
        $method = 'jingdong.vop.goods.checkAreaLimitList';
        return $this->sendRequest($method, $params);
    }

    /**
     * 查询同类商品
     * @param $reqParams
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function getSimilarSkuList($reqParams)
    {
        $method = 'jingdong.vop.goods.getSimilarSkuList';
        return $this->sendRequest($method, $reqParams);
    }

    /**
     * 查询赠品信息
     * @param $skuIdList
     * @param $areaInfo
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function getGiftInfoSkuList($skuIdList, $areaInfo)
    {
        $method = 'jingdong.vop.goods.getGiftInfoSkuList';
        return $this->sendRequest($method, compact('skuIdList', 'areaInfo'));
    }

    /**
     * 验证商品可售性
     * @param $skuIdList
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function checkSkuSaleList($skuIdList)
    {
        $method = 'jingdong.vop.goods.checkSkuSaleList';
        return $this->sendRequest($method, $skuIdList);
    }

    /**
     * 查询商品图片
     * @param $skuIdList
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function getSkuImageList($skuIdList)
    {
        $method = 'jingdong.vop.goods.getSkuImageList';
        return $this->sendRequest($method, $skuIdList);
    }

    /**
     * 根据分类id查询分类信息
     * @param $categoryIdSet
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function getCategoryInfoList($categoryIdSet)
    {
        $method = 'jingdong.vop.goods.getCategoryInfoList';
        return $this->sendRequest($method, $categoryIdSet);
    }

    /**
     * 批量查询商品售卖价
     * @param $skuIdList
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function getSellPrice($skuIdList)
    {
        $method = 'jingdong.vop.goods.getSellPrice';
        return $this->sendRequest($method, $skuIdList);
    }

    /**
     * 查询延保信息
     * @param $skuIdList
     * @param $areaInfo
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function getYanbaoSkuList($skuIdList, $areaInfo)
    {
        $method = 'jingdong.vop.goods.getYanbaoSkuList';
        return $this->sendRequest($method, compact('skuIdList', 'areaInfo'));
    }

    /**
     * 查询子类目列表
     * @param $reqParams
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function getChildCategoryList($reqParams)
    {
        $method = 'jingdong.vop.goods.getChildCategoryList';
        return $this->sendRequest($method, $reqParams);
    }

    /**
     * 查询商品上下架状态
     * @param $skuIdList
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function getSkuStateList($skuIdList)
    {
        $method = 'jingdong.vop.goods.getSkuStateList';
        return $this->sendRequest($method, $skuIdList);
    }

    /**
     * 拍照购API
     * @param $reqParams
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function imageSimilarSku($reqParams)
    {
        $method = 'jingdong.vop.goods.imageSimilarSku';
        return $this->sendRequest($method, $reqParams);
    }
}