<?php

namespace Jsrx\JdVopSdk\Service;

use Psr\SimpleCache\InvalidArgumentException;
use Throwable;

/**
 * VOP-订单API
 * @class   Order
 * @package Jsrx\JdVopSdk\Service
 */
class Order extends BasicService
{
    /**
     * 更新订单扩展信息
     * @param $orderExtInfoList
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function updateCustomOrderExt($orderExtInfoList)
    {
        $method = 'jingdong.vop.order.updateCustomOrderExt';
        return $this->sendRequest($method, $orderExtInfoList);
    }

    /**
     * 查询对账单内的订单详情
     * @param $pageIndex
     * @param $pageSize
     * @param $billNo
     * @param $multiPinSource
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function queryBillsOrderDetails($pageIndex, $pageSize, $billNo, $multiPinSource)
    {
        $method = 'jingdong.vop.order.queryBillsOrderDetails';
        return $this->sendRequest($method, compact('pageIndex', 'pageSize', 'billNo', 'multiPinSource'));
    }

    /**
     * 礼品卡查询
     * @param $pageIndex
     * @param $pageSize
     * @param $orderId
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function getGiftCardsWithPage($pageIndex, $pageSize, $orderId)
    {
        $method = 'jingdong.vop.order.getGiftCardsWithPage';
        return $this->sendRequest($method, compact('pageIndex', 'pageSize', 'orderId'));
    }

    /**
     * 对账单列表
     * @param $pageIndex
     * @param $pageSize
     * @param $multiPinSource
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function queryBillsByPage($pageIndex, $pageSize, $multiPinSource)
    {
        $method = 'jingdong.vop.order.queryBillsByPage';
        return $this->sendRequest($method, compact('pageIndex', 'pageSize', 'multiPinSource'));
    }

    /**
     * 查询配送预计送达时间
     * @param $skuNum
     * @param $skuId
     * @param $areaInfo
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function predictSkuPromise($skuNum, $skuId, $areaInfo)
    {
        $method = 'jingdong.vop.order.predictSkuPromise';
        return $this->sendRequest($method, compact('skuNum', 'skuId', 'areaInfo'));
    }

    /**
     * 批量校验订单是否可取消
     * @param $jdOrderIdSet
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function batchCheckCancelOrder($jdOrderIdSet)
    {
        $method = 'jingdong.vop.order.batchCheckCancelOrder';
        return $this->sendRequest($method, $jdOrderIdSet);
    }

    /**
     * 查询订单详情
     * @param $thirdOrderId
     * @param $jdOrderId
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function queryOrderDetail($thirdOrderId, $jdOrderId)
    {
        $method = 'jingdong.vop.order.queryOrderDetail';
        return $this->sendRequest($method, compact('thirdOrderId', 'jdOrderId'));
    }

    /**
     * 更新订单的po单号
     * @param $poNo
     * @param $thirdOrderId
     * @param $jdOrderId
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function updatePoNoByOrder($poNo, $thirdOrderId, $jdOrderId)
    {
        $method = 'jingdong.vop.order.updatePoNoByOrder';
        return $this->sendRequest($method, compact('poNo', 'thirdOrderId', 'jdOrderId'));
    }

    /**
     * 订单确认收货
     * @param $thirdOrderId
     * @param $jdOrderId
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function confirmReceiveByOrder($thirdOrderId, $jdOrderId)
    {
        $method = 'jingdong.vop.order.confirmReceiveByOrder';
        return $this->sendRequest($method, compact('thirdOrderId', 'jdOrderId'));
    }

    /**
     * 根据订单状态分页查询
     * @param $startDate
     * @param $pageSize
     * @param $endDate
     * @param $pageIndex
     * @param $jdOrderIdIndex
     * @param $checkOrderType
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function checkOrderPageByState(
        $startDate,
        $pageSize,
        $endDate,
        $pageIndex,
        $jdOrderIdIndex,
        $checkOrderType
    ) {
        $method = 'jingdong.vop.order.checkOrderPageByState';
        return $this->sendRequest(
            $method,
            compact('startDate', 'pageSize', 'endDate', 'pageIndex', 'jdOrderIdIndex', 'checkOrderType')
        );
    }

    /**
     * 查询配送信息
     * @param array $params
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function queryDeliveryInfo(array $params)
    {
        $method = 'jingdong.vop.order.queryDeliveryInfo';
        return $this->sendRequest($method, $params);
    }

    /**
     * 确认订单
     * @param array $params
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function confirmOrder(array $params)
    {
        $method = 'jingdong.vop.order.confirmOrder';
        return $this->sendRequest($method, $params);
    }

    /**
     * 取消订单
     * @param array $params
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function cancelOrder(array $params)
    {
        $method = 'jingdong.vop.order.cancelOrder';
        return $this->sendRequest($method, $params);
    }

    /**
     * 查询余额变动明细
     * @param array $params
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function checkBalanceChangeInfo(array $params)
    {
        $method = 'jingdong.vop.order.checkBalanceChangeInfo';
        return $this->sendRequest($method, $params);
    }

    /**
     * 查询账户余额
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function checkAccountBalance()
    {
        $method = 'jingdong.vop.order.checkAccountBalance';
        return $this->sendRequest($method);
    }

    /**
     * 查询商品运费
     * @param $params
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function querySkuFreight($params)
    {
        $method = 'jingdong.vop.order.querySkuFreight';
        return $this->sendRequest($method, ['freightQueryOpenReq' => $params]);
    }

    /**
     * 提交订单
     * @param $reqParams
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function submitOrder($reqParams)
    {
        // 必填参数完整性校验
        $this->checkRequireParams($reqParams, [
            'submitStateType',
            'thirdOrderId',
            'skuInfoList',
            'paymentInfo',
            'consigneeInfo',
            'invoiceInfo'
        ]);

        $method = 'jingdong.vop.order.submitOrder';
        return $this->sendRequest($method, ['submitOrderOpenReq' => $reqParams]);
    }

    /**
     * 查询电子签单服务
     * @param $reqParams
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function getOrderReceiptUrl($reqParams)
    {
        $method = 'jingdong.vop.order.getOrderReceiptUrl';
        return $this->sendRequest($method, $reqParams);
    }
}