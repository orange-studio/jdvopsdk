<?php

namespace Jsrx\JdVopSdk\Service;

use DateTime;
use DateTimeZone;
use Exception;
use Jsrx\JdVopSdk\JdVopSdk;
use Jsrx\JdVopSdk\Utils\Curl;
use Jsrx\JdVopSdk\Utils\Helper;
use Jsrx\JdVopSdk\Utils\LogManager;
use Throwable;

/**
 * SDK服务基础支撑类
 * @class BasicService
 * @package Jsrx\JdVopSdk\Service
 */
class BasicService
{
    /**
     * SDK配置信息
     * @var array
     */
    protected $config;

    /**
     * 生产环境域名
     * @var string
     */
    protected $prodDomain = 'https://api.jd.com/routerjson';

    /**
     * 测试环境域名
     * @var string
     */
    protected $devDomain = 'https://api.jd.com/routerjson';

    /**
     * 构造函数
     * @param $config
     */
    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * 分页迭代器
     * @param callable $callback
     * @param array $options
     * @return array
     */
    public function pagingIterator(callable $callback, array $options = []): array
    {
        // 默认配置
        $defaultOptions = [
            'startIndex'   => 1,
            'endIndex'     => null,
            'dataKey'      => 'items',
            'pageTotalKey' => 'pageTotal',
            'pageIndexKey' => 'pageIndex'
        ];
        $options        = array_merge($defaultOptions, $options);

        // 暂存参数
        $isFinish     = false;
        $collection   = [];
        $currentIndex = $options['startIndex'];

        // 进入循环
        while (!$isFinish) {
            // 请求用户定义闭包获取接口结果
            $result = $callback($currentIndex);

            // 如果到达最后一页/数据为空/返回为空/到达指定结束页码 则退出循环
            if (
                !$result || !array_key_exists($options['dataKey'], $result) ||
                $result[$options['pageTotalKey']] == $result[$options['pageIndexKey']] ||
                ($options['endIndex'] && $options['endIndex'] == $result[$options['pageIndexKey']])
            ) {
                $isFinish = true;
            }

            // 如果返回且返回了合法数据
            if ($result && array_key_exists($options['dataKey'], $result)) {
                foreach ($result as $item) {
                    $collection[] = $item[$options['dataKey']];
                }
            }

            // 页码递增
            $currentIndex++;
        }

        // 返回数据集合
        return $collection;
    }

    /**
     * 发送一次接口请求
     * @param string $method 接口方法名称
     * @param array $reqParams 接口请求参数
     * @param string $url 直接指定接口特定地址
     * @throws Throwable
     */
    protected function sendRequest(string $method, array $reqParams = [], string $url = '')
    {
        // 获取日志管理器
        $logMgr = $this->getLogManager();

        try {
            $url     = $this->buildRequestUrl($url);
            $reqData = $this->buildRequestParam($method, $reqParams);

            // 日志点1: 请求基础信息
            $logMgr->appendLog('request_time:', date('Y-m-d H:i:s'));
            $logMgr->appendLog('request_url:', $url);
            $logMgr->appendLog('access_token:', ($reqData['access_token'] ?? ''));
            //            $logMgr->appendLog('请求参数明细', $reqParams);

            // 发起一次Curl请求
            $curl = new Curl;
            $curl->setOpt(CURLOPT_SSL_VERIFYPEER, false);
            $curl->setOpt(CURLOPT_SSL_VERIFYHOST, false);
            $curl->setOpt(CURLOPT_TIMEOUT, $this->config['http_timeout']);
            $curl->setUserAgent('PHP Curl/2.3 Via JdVopSdk v' . JdVopSdk::VERSION);
            $curlResponse = $curl->post($url, $reqData);
            $curl->close();
            // 日志点2: 响应基础信息
            $logMgr->appendLog(
                '响应代码',
                $curlResponse->getHttpStatus() . ' / ' . ($curlResponse->getErrorMessage() ?: 'OK')
            );
            $logMgr->appendLog('response:', $curlResponse->getResponse());

            // 解析返回内容
            $responseData = $this->parseResponse($curlResponse);
            // 接口请求失败
            if (isset($responseData['error_response'])) {
                $errDesc = $responseData['error_response']['en_desc'];
                throw new Exception($errDesc);
            }

            // 返回数据总以响应类名包装 (responce是故意拼错以配对接口)
            $responseClassName = str_replace('.', '_', $method) . '_responce';
            if (!isset($responseData[$responseClassName])) {
                throw new Exception('Business error: Response class name does not meet requirements.');
            }

            // 拆开包装返回内部数据
            $responseData = $responseData[$responseClassName];
            $rpcResult    = $this->getRpcResult($responseData);
            if ($rpcResult['resultCode'] != 0) {
                throw new Exception($rpcResult['resultMessage']);
            }

            return $rpcResult['result'] ?? true;
        } catch (Throwable $throwable) {
            // 记录错误日志
            $logMgr->appendLog('错误代码', $throwable->getCode());
            $logMgr->appendLog('错误信息', $throwable->getMessage());
            $logMgr->appendLog('错误定位', $throwable->getFile() . ' Line:' . $throwable->getLine());
            $logMgr->logSave();

            // 再次抛出给上层
            throw $throwable;
        } finally {
            unset($logMgr);
        }
    }

    /**
     * @throws Exception
     */
    protected function getRpcResult($RpcResult): array
    {
        try {
            $res = [];
            foreach ($RpcResult as $k => $v) {
                if (is_array($v)) {
                    $res = $v;
                    break;
                }
            }
            return $res;
        } catch (Throwable $th) {
            throw new Exception($th->getMessage());
        }
    }

    /**
     * 发起一次Get请求
     * @param string $url 请求地址
     * @param array $queryParams 查询参数
     * @param bool $noParse 无需解析直接返回
     * @throws Exception
     */
    protected function httpGet(string $url, array $queryParams = [], bool $noParse = false)
    {
        // 发起一次Curl请求
        $curl = new Curl;
        $curl->setOpt(CURLOPT_SSL_VERIFYPEER, false);
        $curl->setOpt(CURLOPT_SSL_VERIFYHOST, false);
        $curl->setOpt(CURLOPT_TIMEOUT, $this->config['http_timeout']);
        $curl->setUserAgent('PHP Curl/2.3 Via JdVopSdk v' . JdVopSdk::VERSION);
        $curlResponse = $curl->get($url, $queryParams);

        // 返回已解码的信息
        return $noParse ? $curlResponse : $this->parseResponse($curlResponse);
    }

    /**
     * 检查必填参数是否填写完整
     * @param $checkParams
     * @param array $reqLists
     * @return void
     * @throws Exception
     */
    protected function checkRequireParams($checkParams, array $reqLists = [])
    {
        // 检查参数完整性
        if ($reqLists) {
            foreach ($reqLists as $requireParamName) {
                if (!array_key_exists($requireParamName, $checkParams)) {
                    throw new Exception('Missing required parameter: ' . $requireParamName);
                }
            }
        }
    }

    /**
     * 获取日志管理器
     * @return LogManager
     */
    protected function getLogManager(): LogManager
    {
        return new LogManager([
            'log_file'  => $this->config['log_file'],
            'log_cycle' => $this->config['log_cycle']
        ]);
    }

    /**
     * 构建请求路径
     * @param string $url 给出特定Url则直接使用
     * @return string 按照正式/沙箱配置给出对应Url
     */
    private function buildRequestUrl(string $url = ''): string
    {
        if (empty($url)) {
            return $this->config['is_sandbox'] ? $this->devDomain : $this->prodDomain;
        } else {
            return $url;
        }
    }

    /**
     * 构造请求参数
     * @param string $method
     * @param array $reqParams
     * @return array
     * @throws Exception
     */
    private function buildRequestParam(string $method, array $reqParams = []): array
    {
        // 固定时区以匹配京东服务器
        $dateTime = new DateTime('now', new DateTimeZone('Asia/Shanghai'));

        // 如果不存在令牌则托管获取令牌

        // 组装公共参数
        $commonParams = [
            'method'       => $method,
            'app_key'      => $this->config['app_key'],
            'access_token' => $this->config['access_token'],
            'timestamp'    => $dateTime->format('Y-m-d H:i:s'),
            'format'       => 'json',
            'v'            => '2.0'
        ];
        $reqParams    = ['360buy_param_json' => empty($reqParams) ? '{}' : json_encode($reqParams)];
        // 将请求参数合并入公共参数并构造签名
        $reqParams         = array_merge($commonParams, $reqParams);
        $reqParams['sign'] = $this->buildSignature($reqParams);
        return $reqParams;
    }

    /**
     * 构建请求签名
     * @param array $reqParams
     * @return string
     */
    private function buildSignature(array $reqParams): string
    {
        // 签名临时字符串
        $preSignatureStr = '';

        // 1. 按照参数名进行升序排列
        ksort($reqParams);

        // 2. 连接参数名与参数值
        foreach ($reqParams as $name => $value) {
            $preSignatureStr .= $name;
            $preSignatureStr .= $value;
        }

        // 3. 在前后拼接API秘钥 并进行MD5转大写
        $preSignatureStr = $this->config['app_secret'] . $preSignatureStr . $this->config['app_secret'];
        return strtoupper(md5($preSignatureStr));
    }

    /**
     * 解析响应内容
     * @param Curl $curlResponse
     * @return mixed
     * @throws Exception
     */
    private function parseResponse(Curl $curlResponse)
    {
        // 请求不成功
        if (!$curlResponse->isSuccess()) {
            $httpStatus = $curlResponse->getHttpStatus();
            throw new Exception('Abnormal response status: ' . $httpStatus);
        }

        // 返回的内容为空
        if (empty($curlResponse->getResponse())) {
            throw new Exception('Abnormal response: response body is empty.');
        }

        // 返回数据不合法
        return Helper::jsonDecode($curlResponse->getResponse());
    }
}