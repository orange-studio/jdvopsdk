<?php

namespace Jsrx\JdVopSdk\Service;

use Exception;
use Jsrx\JdVopSdk\Utils\Curl;
use Jsrx\JdVopSdk\Utils\Helper;

/**
 * API授权接口
 * @class AccessToken
 * @package Jsrx\JdVopSdk\Service
 */
class Accesstoken extends BasicService
{
    /**
     * 获取访问授权令牌
     * @return mixed|void
     * @throws Exception
     */
    public function GetAccessToken()
    {
        $logMgr = $this->getLogManager();
        try {
            $logMgr->appendLog('GetAccessToken', 'start');
            // 在后端发起一次授权链接访问
            $authUrl = $this->buildOauthUrl();
            $logMgr->appendLog('buildOauthUrl', $authUrl);
            $response = $this->httpGet($authUrl, [], true);
            $logMgr->appendLog('authUrlresponse', $response);
            if ($response->getHttpStatus() != 302) {
                throw new Exception('Business error: Authorization link request failed.');
            }

            // 页面访问后发起一次302则进入跳转链接 获取Code
            $location = $response->getResponseHeaders('location');
            $logMgr->appendLog('location', $location);
            parse_str(parse_url($location)['query'], $locationQuery);
            if (!array_key_exists('code', $locationQuery)) {
                throw new Exception('Business error: Authorization link does not carry code.');
            }

            // 使用授权Code换取授权令牌并发起缓存 注意: 令牌需要提前一点儿刷新
            $accessToken = $this->code2AccessToken($locationQuery['code']);
            $logMgr->appendLog('code2AccessToken', $accessToken);
            return $accessToken;
        } catch (\Throwable $th) {
            throw new Exception($th->getMessage());
        } finally {
            $logMgr->logSave();
        }
    }


    /**
     * @throws Exception
     */
    public function RefreshToken()
    {
        $logMgr = $this->getLogManager();
        try {
            $logMgr->appendLog('RefreshToken', 'start');
            // 在后端发起一次授权链接访问
            $authUrl = $this->buildOauthUrl();
            $logMgr->appendLog('buildOauthUrl', $authUrl);
            $response = $this->httpGet($authUrl, [], true);
            $logMgr->appendLog('authUrl', $response);
            if ($response->getHttpStatus() != 302) {
                throw new Exception('Business error: Authorization link request failed.');
            }

            // 页面访问后发起一次302则进入跳转链接 获取Code
            $location = $response->getResponseHeaders('location');
            $logMgr->appendLog('location', $location);
            parse_str(parse_url($location)['query'], $locationQuery);
            if (!array_key_exists('code', $locationQuery)) {
                throw new Exception('Business error: Authorization link does not carry code.');
            }

            // 使用授权Code换取授权令牌并发起缓存 注意: 令牌需要提前一点儿刷新
            $accessToken = $this->code2RefreshToken($locationQuery['code']);
            $logMgr->appendLog('code2RefreshToken', $accessToken);
            return $accessToken;
        } catch (\Throwable $th) {
            $logMgr->appendLog('Exception', $th->getMessage());
            throw new Exception($th->getMessage());
        } finally {
            $logMgr->logSave();
        }
    }

    /**
     * @throws Exception
     */
    public function code2RefreshToken($code)
    {
        $logMgr = $this->getLogManager();
        try {
            $logMgr->appendLog('code2RefreshToken', 'start');

            // 换取Token参数
            $refreshParams = [
                'app_key'       => $this->config['app_key'],
                'app_secret'    => $this->config['app_secret'],
                'grant_type'    => 'refresh_token',
                'refresh_token' => $this->config['refresh_token']
            ];
            $baseUrl       = 'https://open-oauth.jd.com/oauth2/refresh_token';
            $url           = $baseUrl . '?' . http_build_query($refreshParams);
            $logMgr->appendLog('code2RefreshTokenUrl', $url);
            // 发起一次Get请求来获取信息
            $responseData = $this->httpGet($url);
            $logMgr->appendLog('response', $responseData);
            if ($responseData['code'] != 0) {
                throw new Exception('RefreshToken Code to session error[' . $responseData['code'] . ']: ' . $responseData['msg']);
            }

            return $responseData;
        } catch (\Throwable $th) {
            throw new Exception($th->getMessage());
        } finally {
            $logMgr->logSave();
        }
    }

    /**
     * 构建授权Url
     * @return string
     * @throws Exception
     */
    public function buildOauthUrl(): string
    {
        // 密码进行加密
        $urlPassword = Helper::encodeBase64URLSafe(
            Helper::rsaEncryptWithOpenSSL($this->config['private_key'], md5($this->config['vop_password']))
        );

        // 开放授权参数
        $oauthParams = [
            'app_key'       => $this->config['app_key'],
            'redirect_uri'  => $this->config['redirect_uri'],
            'username'      => $this->config['vop_username'],
            'password'      => $urlPassword,
            'response_type' => 'code',
            'scope'         => 'snsapi_base'
        ];

        // 构建授权链接并返回
        $baseUrl = 'https://open-oauth.jd.com/oauth2/authorizeForVOP';
        return $baseUrl . '?' . http_build_query($oauthParams);
    }

    /**
     * 使用Code换取AccessToken
     * @param $code
     * @return Curl|mixed
     * @throws Exception
     */
    public function code2AccessToken($code)
    {
        // 换取Token参数
        $oauthParams = [
            'app_key'    => $this->config['app_key'],
            'app_secret' => $this->config['app_secret'],
            'grant_type' => 'authorization_code',
            'code'       => $code
        ];

        // 发起一次Get请求来获取信息
        $responseData = $this->httpGet('https://open-oauth.jd.com/oauth2/access_token', $oauthParams);
        if ($responseData['code'] != 0) {
            throw new Exception('Code to session error[' . $responseData['code'] . ']: ' . $responseData['msg']);
        }

        return $responseData;
    }
}