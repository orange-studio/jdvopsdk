<?php

namespace Jsrx\JdVopSdk\Service;

use Psr\SimpleCache\InvalidArgumentException;
use Throwable;

/**
 * VOP-地址API
 * @class Address
 * @package Jsrx\JdVopSdk\Service
 */
class Address extends BasicService
{
    /**
     * 查询四级地址ID列表
     * @param array $reqParams
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function queryJdAreaIdList(array $reqParams = ['areaLevel' => 1])
    {
        $method = 'jingdong.vop.address.queryJdAreaIdList';
        return $this->sendRequest($method, $reqParams);
    }

    /**
     * 验证四级地址ID有效性
     * @param array $reqParams
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function verifyAreaFourIdOpenReq(array $reqParams)
    {
        $method = 'jingdong.vop.address.verifyAreaFourIdOpenReq';
        return $this->sendRequest($method, $reqParams);
    }

    /**
     * 地址详情转换京东地址编码
     * @param $addressDetailStr
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function convertFourAreaByDetailStr($addressDetailStr)
    {
        $method = 'jingdong.vop.address.convertFourAreaByDetailStr';
        return $this->sendRequest($method, compact('addressDetailStr'));
    }

    /**
     * 通过经纬度转换为四级地址
     * @param array $reqParams
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function convertFourAreaByLatLng(array $reqParams)
    {
        $method = 'jingdong.vop.address.convertFourAreaByLatLng';
        return $this->sendRequest($method, $reqParams);
    }
}