<?php

namespace Jsrx\JdVopSdk;

use Exception;
use Jsrx\JdVopSdk\Service\Afs;
use Jsrx\JdVopSdk\Service\Accesstoken;
use Jsrx\JdVopSdk\Service\Address;
use Jsrx\JdVopSdk\Service\Bbc;
use Jsrx\JdVopSdk\Service\Goods;
use Jsrx\JdVopSdk\Service\Order;
use Jsrx\JdVopSdk\Service\Stock;
use Jsrx\JdVopSdk\Service\Message;

/**
 * 京东VOP开放平台SDK
 * @class JdVopS
 * @package Jsrx\JdVopSdk
 * @property Accesstoken $Accesstoken VOP-授权服务
 * @property Address $address VOP-地址API
 * @property Goods $goods VOP-商品API
 * @property Order $order VOP-订单API
 * @property Stock $stock VOP-库存API
 * @property Bbc $bbc BBC-通用场景接口
 * @property Message $message VOP-消息API
 */
class JdVopSdk
{
    /**
     * 当前Sdk版本号
     * @var string
     */
    const VERSION = '1.0.0';

    /**
     * 默认配置
     * @var array
     */
    protected $config = [
        // 是否沙箱环境
        'is_sandbox'   => false,
        // 请求超时时间(秒)
        'http_timeout' => 10,
        // 应用KEY(应用总览中可查看)
        'app_key'      => '',
        // 应用密钥	(应用总览中可查看)
        'app_secret'   => '',
        // 接口调用凭证(留空可以自动托管获取/传入则使用指定)
        'access_token' => '',
        // 刷新token凭证
        'refresh_token' => '',
        // VOP企业采购账号用户
        'vop_username' => '',
        // VOP企业采购账号密码
        'vop_password' => '',
        // 授权跳转地址(需要与应用配置域名在同域下)
        'redirect_uri' => '',
        // 应用私钥信息
        'private_key'  => '',
        // 缓存驱动类(如不指定使用默认文件缓存)
        'cache'        => '',
        // 是否存储请求日志，传入绝对路径，false关闭日志
        'log_file'     => false,
        // 日志周期，超期自动清理(单位:天)
        'log_cycle'    => 90,
    ];

    /**
     * 构造函数
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $config)
    {
            $this->config = array_merge($this->config, $config);
            if (empty($this->config['log_path'])) {
                $this->config['log_path'] = dirname(__DIR__) . '/Log/JdVopSdk';
            }
    }

    /**
     * 获取配置信息
     */
    public function getConfig(): array
    {
        return $this->config;
    }

    public function setAccessToken(string $AccessToken): JdVopSdk
    {
        $this->config['access_token'] = $AccessToken;
        return $this;
    }

    public function setRefreshToken(string $RefreshToken): JdVopSdk
    {
        $this->config['refresh_token'] = $RefreshToken;
        return $this;
    }
    /**
     * 魔术方法: 获取服务类
     * @param string $name 服务类名称
     * @return mixed
     * @throws Exception
     */
    public function __get(string $name)
    {
        // 当前需求服务的类名
        $serviceName    = ucfirst(strtolower($name));
        $classNamespace = "\\Jsrx\\JdVopSdk\\Service\\" . $serviceName;

        // 服务不存在则异常
        if (!class_exists($classNamespace)) {
            throw new Exception("The service {$serviceName} does not exist.");
        }

        // 实例化类并返回
        return new $classNamespace($this->config);
    }
}